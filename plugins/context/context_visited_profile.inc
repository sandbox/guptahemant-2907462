<?php

/**
 * @file
 * Check if the visited user profile has one of the selected roles.
 */

/**
 * Extends the base context conditon class.
 */
class ContextVisitedProfile extends context_condition {

  /**
   * This will define the roles which can be use for settings form.
   */
  public function condition_values() {
    $values = array();
    foreach (user_roles() as $rid => $role_name) {
      if ($rid == DRUPAL_ANONYMOUS_RID) {
        // Do not show a role option for anonymous user.
      }
      elseif ($rid == DRUPAL_AUTHENTICATED_RID) {
        $values['authenticated user'] = check_plain($role_name);
      }
      else {
        $values[$role_name] = check_plain($role_name);
      }
    }
    return $values;
  }

  /**
   * This function will check and evaluate the context condition.
   */
  public function execute($account) {
    $roles = $account->roles;
    foreach ($roles as $rid => $role) {
      foreach ($this->get_contexts($role) as $context) {
        $this->condition_met($context, $role);
      }
    }
  }

}
